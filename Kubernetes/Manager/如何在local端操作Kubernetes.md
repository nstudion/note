# 管理Kubernetes

### 1. 如何在local 管理端操作 K8S

Step1. set env to ~/.zshrc or ~/.bash_profile and source file or reboot your computer

```bash
K8S_TOKEN={TOKEN}
```

Step2.exec bash

```bash
kubectl config set-credentials default --token=$K8S_TOKEN
kubectl config set-cluster default --server=https://{K8S API URL} --insecure-skip-tls-verify=true
kubectl config set-context default --cluster=default --user=default
kubectl config use-context default
```

### 2.如何使用local 端連接k8s 上的例如 redis or 其他服務

說明: 可以透過 kubectl port-forward  使用 localhost or local IP  指定port，連接到k8s 上的某項服務，以下使用redis 為例。

```bash
kubectl -n dev port-forward <redis pod> --address 0.0.0.0  <local custom port>:6379
```

**備註: 連線假設過一段時間會中斷，所以需要再次連接。**