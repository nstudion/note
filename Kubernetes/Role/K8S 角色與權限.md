# K8S 角色與權限

說明: 這邊會說明，K8S 角色與權限，建立、刪除、拿取Token。(這邊會以RBAC為主)

[https://kubernetes.io/docs/reference/access-authn-authz/authorization/](https://kubernetes.io/docs/reference/access-authn-authz/authorization/)

[https://kubernetes.io/docs/reference/access-authn-authz/rbac/](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)

### 1. 如何建立角色

說明: 當建立角色需要了解K8S 的 Role 、ClusterRole 、RoleBinding、Service Account…，這些關係，關係可以參考下圖，Role Binding 會將 Role 與 Service Account 做綁定。

![截圖 2021-09-22 下午5.54.50.png](./Role.png)

- Role/ClusterRole 可以建立角色並定義規則。[https://kubernetes.io/docs/reference/access-authn-authz/rbac/#role-and-clusterrole](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#role-and-clusterrole)
- Group/Service Account Account 主體或Group。
- RoleBinding/ClusterRoleBinding 可以將 Service Account 與 Role 做綁定。

step1. 建立角色

- Role Role 一定要綁定 namespace ，它是定義某一個 namespace 底下的權限。(範例是建立 dev-test-role 賦予權限)
    
    ```yaml
    apiVersion: rbac.authorization.k8s.io/v1
    kind: Role
    metadata:
      namespace: dev
      name: dev-test-role
    rules:
    - apiGroups: [""] # "" indicates the core API group
      resources: ["pods","ingresses","secrets","configmaps","deployments","services","endpoints"]
      verbs: ["get", "watch", "list", "create", "update", "patch", "delete"]
    ```
    
- ClusterRole ClusterRole 可以跨namespace ，是定義Cluster 層級的權限。(範例是建立cluster role cluster_role_test)

step2. 建立 Service Account

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: test-service-account
  namespace: dev
  labels:
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: Reconcile
```

step3. 建立 Role Binding

- Role Binding
    
    ```yaml
    kind: RoleBinding
    apiVersion: rbac.authorization.k8s.io/v1beta1
    metadata:
      name: dev-test
      annotations:
        rbac.authorization.kubernetes.io/autoupdate: "true"
    roleRef:
      kind: Role
      name: dev-test-role
      apiGroup: rbac.authorization.k8s.io
    subjects:
    - kind: ServiceAccount
      name: test-service-account
      namespace: dev
    ```
    
- Cluster Role Binding (cluster-admin 是 K8S 預設最高權限的角色設定)
    
    ```yaml
    kind: ClusterRoleBinding
    apiVersion: rbac.authorization.k8s.io/v1beta1
    metadata:
      name: admin
      annotations:
        rbac.authorization.kubernetes.io/autoupdate: "true"
    roleRef:
      kind: ClusterRole
      name: cluster-admin
      apiGroup: rbac.authorization.k8s.io
    subjects:
    - kind: ServiceAccount
      name: admin
      namespace: kube-system
    ```
    

### 2. 如何拿到Access Token

說明: 以上述的 test-service-account 為例。

step1. 取得 service account 資訊

```bash
kubectl -n dev get serviceaccounts test-service-account -o yaml
```

result

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"ServiceAccount","metadata":{"annotations":{},"labels":{"addonmanager.kubernetes.io/mode":"Reconcile","kubernetes.io/cluster-service":"true"},"name":"test-service-account","namespace":"dev"}}
  creationTimestamp: "2021-09-23T06:45:28Z"
  labels:
    addonmanager.kubernetes.io/mode: Reconcile
    kubernetes.io/cluster-service: "true"
  name: test-service-account
  namespace: dev
  resourceVersion: "86491660"
  selfLink: /api/v1/namespaces/dev/serviceaccounts/test-service-account
  uid: f7437089-44a8-48d2-bc89-adcca4f250df
secrets:
- name: test-service-account-token-t5zkc
```

step2. 複製secrets 的name 去做 describe 指令

```bash
kubectl -n dev describe secrets test-service-account-token-t5zkc
```

result

```
Name:         test-service-account-token-t5zkc
Namespace:    dev
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: test-service-account
              kubernetes.io/service-account.uid: f7437089-44a8-48d2-bc89-adcca4f250df

Type:  kubernetes.io/service-account-token

Data
====
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6Ik8zTW5yZFdrZy1NQ29wVXNkeTd4ZWpkbzNQN0xwNVZ5bmhhQTc5cVo4UEkifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZXYiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlY3JldC5uYW1lIjoidGVzdC1zZXJ2aWNlLWFjY291bnQtdG9rZW4tdDV6a2MiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoidGVzdC1zZXJ2aWNlLWFjY291bnQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJmNzQzNzA4OS00NGE4LTQ4ZDItYmM4OS1hZGNjYTRmMjUwZGYiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6ZGV2OnRlc3Qtc2VydmljZS1hY2NvdW50In0.hy50_3F4HIs_WKzTG7ojscZm2idXfVnTkuJtlpx82K9Rgdgki5XPfVjrEvzOdDWD8dBL8nIZEDY7BQEjPqQ0_Y7N8jaAsRBoD853i8wImh4ahdRKB_0B3da1GYEYcpvrcr8sL58rwg6SBZsJMnv4bbkmYEaZoIT8npQVmvd2mg6zYK3b0xyuP7yd1OQkkBt_c-tiwxjPSDGtyYT7z8TBT3MLsaWq6WK4yzGX--ZpMOPbEEFxTFY5qtmuCmVvuG_FFXQROuI510go4zqxLfbb_NdvgU5pTQGNyT9hpXYiVvc9QFVt0OAF9w79TKaF3-O-hXVaKgtPu0Useov04Y33zg
ca.crt:     1042 bytes
namespace:  3 bytes
```