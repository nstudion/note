# Secret 加密方式

由於Kubernete Secret 只使用base64 編碼，所以直接放到Git上，會有資安風險，所以找了一個針對Secret加密工具 Sealed Secrets 詳細請參考底下網址。
[Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets)

### 1. Installation

Step1. CLI Install

```bash 
brew install kubeseal
```
或
```bash
wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.3/kubeseal-0.17.3-darwin-amd64.tar.gz
tar -xf kubeseal-0.17.3-darwin-amd64.tar.gz
chmod +rxw kubeseal
mv kubeseal /usr/local/bin
```
Test CLI
```bash
kubeseal --help
```
Result
```bash
Usage of kubeseal:
      --add_dir_header                   If true, adds the file directory to the header of the log messages
      --allow-empty-data                 Allow empty data in the secret object
      --alsologtostderr                  log to standard error as well as files
      --as string                        Username to impersonate for the operation
      --as-group stringArray             Group to impersonate for the operation, this flag can be repeated to specify multiple groups.
      --cert string                      Certificate / public key file/URL to use for encryption. Overrides --controller-*
      --certificate-authority string     Path to a cert file for the certificate authority
      --client-certificate string        Path to a client certificate file for TLS
      --client-key string                Path to a client key file for TLS
      --cluster string                   The name of the kubeconfig cluster to use
      --context string                   The name of the kubeconfig context to use
      --controller-name string           Name of sealed-secrets controller. (default "sealed-secrets-controller")
      --controller-namespace string      Namespace of sealed-secrets controller. (default "kube-system")
      --fetch-cert                       Write certificate to stdout. Useful for later use with --cert
  -o, --format string                    Output format for sealed secret. Either json or yaml (default "json")
      --from-file strings                (only with --raw) Secret items can be sourced from files. Pro-tip: you can use /dev/stdin to read pipe input. This flag tries to follow the same syntax as in kubectl
      --insecure-skip-tls-verify         If true, the server's certificate will not be checked for validity. This will make your HTTPS connections insecure
      --kubeconfig string                Path to a kube config. Only required if out-of-cluster
      --log_backtrace_at traceLocation   when logging hits line file:N, emit a stack trace (default :0)
      --log_dir string                   If non-empty, write log files in this directory
      --log_file string                  If non-empty, use this log file
      --log_file_max_size uint           Defines the maximum size a log file can grow to. Unit is megabytes. If the value is 0, the maximum file size is unlimited. (default 1800)
      --logtostderr                      log to standard error instead of files (default true)
      --merge-into string                Merge items from secret into an existing sealed secret file, updating the file in-place instead of writing to stdout.
      --name string                      Name of the sealed secret (required with --raw and default (strict) scope)
  -n, --namespace string                 If present, the namespace scope for this CLI request
      --one_output                       If true, only write logs to their native severity level (vs also writing to each lower severity level)
      --password string                  Password for basic authentication to the API server
      --raw                              Encrypt a raw value passed via the --from-* flags instead of the whole secret object
      --re-encrypt                       Re-encrypt the given sealed secret to use the latest cluster key.
      --recovery-private-key strings     Private key filename used by the --recovery-unseal command. Multiple files accepted either via comma separated list or by repetition of the flag. Either PEM encoded private keys or a backup of a json/yaml encoded k8s sealed-secret controller secret (and v1.List) are accepted. 
      --recovery-unseal                  Decrypt a sealed secrets file obtained from stdin, using the private key passed with --recovery-private-key. Intended to be used in disaster recovery mode.
      --request-timeout string           The length of time to wait before giving up on a single server request. Non-zero values should contain a corresponding time unit (e.g. 1s, 2m, 3h). A value of zero means don't timeout requests. (default "0")
      --scope string                     Set the scope of the sealed secret: strict, namespace-wide, cluster-wide (defaults to strict). Mandatory for --raw, otherwise the 'sealedsecrets.bitnami.com/cluster-wide' and 'sealedsecrets.bitnami.com/namespace-wide' annotations on the input secret can be used to select the scope. (default "strict")
  -w, --sealed-secret-file string        Sealed-secret (output) file
  -f, --secret-file string               Secret (input) file
      --server string                    The address and port of the Kubernetes API server
      --skip_headers                     If true, avoid header prefixes in the log messages
      --skip_log_headers                 If true, avoid headers when opening log files
      --stderrthreshold severity         logs at or above this threshold go to stderr (default 2)
      --tls-server-name string           If provided, this name will be used to validate server certificate. If this is not provided, hostname used to contact the server is used.
      --token string                     Bearer token for authentication to the API server
      --user string                      The name of the kubeconfig user to use
      --username string                  Username for basic authentication to the API server
  -v, --v Level                          number for the log level verbosity
      --validate                         Validate that the sealed secret can be decrypted
      --version                          Print version information and exit
      --vmodule moduleSpec               comma-separated list of pattern=N settings for file-filtered logging
pflag: help requested
```

Step2.Install Controller
```bash
wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.3/controller.yaml
kubectl apply -f controller.yaml
```

Step3. Test

3.1 create secret file
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: test-secret
type: Opaque
data:
  username: dXNlcm5hbWU=
  password: cGFzc3dvcmQ=
```

3.2 Encrypt
```bash
kubeseal --format=yaml < secret.yaml > sealed-secret.yaml
```

3.2 Output sealed-secret.yaml

```yaml
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  creationTimestamp: null
  name: test-secret
  namespace: default
spec:
  encryptedData:
    password: AgAjMTvSwymwb039EY7bJg8qk94S20vIaIpMkTWQSUb98Fd1CKw0sQimoHvwKc/ngiTQbmHKZQpybhGX14Ktx1/eCRE6pUpJ3R5yI+xxkN8r//RXFztzLRPXA02kfA3PdstN7nsbtQqgkJKYzXKPutsY5UlicjI3UzI3CIqOfrc7vuVpT7UxKuSQh7/JRJKdz4SnxEpKvUSgNVQ26xIs+JP97+jL/wUr8PSwBDHhRSdd/mlZr3wRlUcrkS38ILB4QZWnFa7gpMsg+UmF00fFKPJyR5awpJD2QKmmnKXee8dsKffP7uzRzCHCo46d+Q6BKyKmqlCRCpGU+TQkuHm39N9wrZ7DmFVvGZbC7wHLWgsenVURJaazmNXf30hCKQKaefH1SzAVNRDiY3PIzMF8ei/gcujxjctRXnmb7zo+XnRkKKUkZHCs0gZHc88u3WT3gccxSAhBfRldjFSG3OHhx0AHaMvgXkTrZmY4Uo2Ql4kBKQSoYsv/s3yCZ68n33Yuic9SCbAw+AJrF75vT4eQsj1WgqNCXCNNfF+CC9OJ09St2vzhjIxT5AOS4C2QPjFUgOOOg5L0OaL/B8NQbO60TtXR8OviqDfJ6wwU6yeMINbucU+abU7y9RCzhIIADQf0954xx1tPPALYhNozy4ZI7JuXM3PbBenD/Vsbllazt22so3BX6ARqoX5PJOE0HasPs/UhTh3QIXrlWg==
    username: AgCkoZAxO/posNI2ocAW9/3ozZk7X8aXcU41OCTw2ZptTYTo8d/gb5z3u4UPCWPiXQvn/cdef9McqdSwsSb78JyFxrDavDyM69Ugqj0QyBe2Dk7M1ZdZrbmzKVwj+X9673XTG3TytAAxqLQtPtOFhG7GOwwc/dx9NbEOErdVb5Akn2Lz73AA2N60LvDSOTF3Fc9MYPYhL82NBfnae/lmsJh8Plcx4zkMPCuLNEZF6DebkRZJkLZM6pWgxMpxjn5yiJb2a1/kFC8RPGmxmiI2m8c1xY3RTcsPdaagGhbigcaPNp5N4ToIlmwUu3rQSo98KIELU8D6DLqw7UGr6trx+zpK5mDo3EthKL8e3+27G28NZAgNgyIUFdU2DdeMEPfs6kuvDm+eXN0oX/6+gDADy7vS1dFRI65WyN2yzsT2fyIPCY7I2bDZUtjSc0NcwZTlfb4Ei+zN7bsHC/Bca9fZgKuC+TowS4eIiVy35LG74VPqW3s+mlTPFVa/ImlFR1SiQIjbmP3ZaPokkV7eElALsuL789vZIwIfhKBxof7C5iFY+WP7cQ4tSCAbsXkST9XU4E2kCtAcHkJh5g7od27P8qBaNP+OEzP7aNvSjtTV1+kETLMLGMPB3B8rOr+wnAE9NljwG8zfu+ASlPgng2cLl7ltf09iov/zg8Jx3Y6qC+DLJm7QIBQkzzKlIbp1wVbp4Hi16+LUXA4hVA==
  template:
    data: null
    metadata:
      creationTimestamp: null
      name: test-secret
      namespace: default
    type: Opaque
```

3.3 Apply sealed-secret.yaml

```bash
kubectl apply -f sealed-secret.yaml
```

Verify

```bash
kubectl get secret test-secret -o yaml
```
Result

```yaml
apiVersion: v1
data:
  password: cGFzc3dvcmQ=
  username: dXNlcm5hbWU=
kind: Secret
metadata:
  creationTimestamp: "2022-02-10T07:13:11Z"
  name: test-secret
  namespace: default
  ownerReferences:
  - apiVersion: bitnami.com/v1alpha1
    controller: true
    kind: SealedSecret
    name: test-secret
    uid: 83a4f301-2bb5-4996-acd7-4bda3857b519
  resourceVersion: "5247515"
  selfLink: /api/v1/namespaces/default/secrets/test-secret
  uid: d8502c13-1733-4661-90e9-91164efe5b1e
type: Opaque
```

